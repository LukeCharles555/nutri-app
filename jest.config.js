module.exports = {
  testMatch: ["**/*.(test|spec).+(js|jsx|ts|tsx)"],
  setupFilesAfterEnv: ["./tests/setup-env.js"],
  modulePathIgnorePatterns: ["<rootDir>/node_modules"],
  collectCoverage: true,
  collectCoverageFrom: [
    "src/**/*.{js,jsx}",
    "!**node_modules/**",
    "!**vendor/**",
    "!src/**/**.styles.js",
    "!src/*.{js,jsx}",
    "!src/theme/*",
    "!testData/*",
  ],
  coverageDirectory: "coverage",
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
  moduleNameMapper: {
    "\\.(png|ico)$": "<rootDir>/__mocks__/fileMock.js",
  },
  snapshotSerializers: ["enzyme-to-json/serializer"],
};
